CREATE DATABASE db_task;
\c db_task;
CREATE TABLE price_paid
(
   transaction_id   VARCHAR (255) NOT NULL ,
   price            INT,
   date_transfer    DATE,
   postcode         VARCHAR (255),
   property_type    VARCHAR (1),
   old_new          VARCHAR (1),
   duration         VARCHAR (1),
   paon             VARCHAR (255),
   saon             VARCHAR (255),
   street           VARCHAR (255),
   locality         VARCHAR (255),
   town_city        VARCHAR (255),
   district         VARCHAR (255),
   county           VARCHAR (255),
   ppd              VARCHAR (1),
   record           VARCHAR (1)
);

COPY price_paid (transaction_id, price, date_transfer, postcode, property_type, old_new, duration, paon, saon, street, locality, town_city, district, county, ppd, record ) FROM '/var/lib/postgresql/csv/pp-complete.csv' DELIMITER ',' CSV;
