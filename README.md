Instructions: 
1. create folder postgresql/csv
2. download http://prod.publicdata.landregistry.gov.uk.s3-website-eu-west-1.amazonaws.com/pp-complete.csv
3. make sure the file downloaded with name pp-complete.csv is located in the postgresql/csv directory
4. run: sudo docker-compose -f create_containers.yml up -d --force-recreate

go to app/README.md for more information about the app

JCN
