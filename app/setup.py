from setuptools import setup, Command

VERSION="1.0.0"

setup(
    name='apiRest',
    version=VERSION,
    author='Jorge CN',
    author_email='jcn284@nyu.edu',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    python_requires='>=3.7',
    package_data={'':['lib/cfg/*cfg']},
    include_package_data=True,
    packages=[
        'apiRest',
        'apiRest.apis',
        'apiRest.apis.utils',
        'apiRest.lib',
    ],
)
