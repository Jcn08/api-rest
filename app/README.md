# API Rest
- API Restful to query data from database for a frontend app
- Framework: Flask, lib:RestPlus, Logging:On
- This project is fully documented with swagger
- The API is made with blueprints and 2 endpoints
- App running on a simple instance on AWS. Performance will be affected <-- any query takes 60sec on this free AWS tier. On your computer, it takes less than a second
- CSV is loaded into postgresql database using init.sql file
- To test the API go to: http://15.237.53.136:5000/api_v1.0/
Then, click on **graph** and **try it out!!!**


### Project Architecture:
Microservices --> 3 docker containers:
- postgres: container with postgresql database
- adminer: container with database client to access the postgre ddbb
- web: api rest

### App Architecture
Clean architecture: from inner layer(1) to outer layer(5)
1. flask app
2. blueprint + apis
3. queries
4. result format
5. logging (cross)

### TODO:
- Use pyspark.sql + Dataframes --> Queries take too long
- More test XD

### info:
- For graph two, the price bins have been calculated using the method "histogram_bin_edges" from library numpy.
