# -*- encoding: utf-8 -*-
import psycopg2

from psycopg2 import sql
from apiRest.lib.config import config
from apiRest.lib import my_logging as log

from dateutil.relativedelta import relativedelta


def connect(func):
    def with_connection_(*args,**kwargs):
        """ Connect to the PostgreSQL database server """
        conn = None
        rv=list
        try:
            # read connection parameters
            params = config()

            # connect to the PostgreSQL server
            log.logging.info('Connecting to the PostgreSQL database...')
            conn = psycopg2.connect(**params)
            
            rv = func(conn, *args,**kwargs)
            log.logging.info(rv)
        
        except (Exception, psycopg2.DatabaseError) as error:
            log.logging.error(error)
        
        finally:
            if conn is not None:
                conn.close()
                log.logging.info('Database connection closed.')
        
        return rv
    return with_connection_

@connect
def query_graph_one(conn, postcode, startDate, endDate=None): 
    """ Runs the query for the graph One """
    #TODO:TRY pyspark.sql https://stackoverflow.com/a/56279311
    cur = conn.cursor()
    log.logging.info(f'Searching for properties with postcode:{postcode} from {startDate} to {endDate}')
    
    # format query
    query = sql.SQL("SELECT EXTRACT(YEAR FROM date_transfer), EXTRACT(MONTH FROM date_transfer), property_type, avg(price) \
                    FROM price_paid \
                    where date_transfer between (%s) and (%s) \
                    and postcode like (%s) \
                    GROUP BY EXTRACT(YEAR FROM date_transfer), EXTRACT(MONTH FROM date_transfer), property_type \
                    ORDER BY property_type, EXTRACT(YEAR FROM date_transfer), EXTRACT(MONTH FROM date_transfer) ASC")
    log.logging.debug(query.as_string(conn))
    
    # run query
    cur.execute(query, [startDate.strftime("%m/%d/%Y"), endDate.strftime("%m/%d/%Y"), '%' + postcode + '%'])

    # display the PostgreSQL database server version
    result = cur.fetchall()
    log.logging.info(result)
    return result


@connect
def query_graph_two(conn, postcode, given_date=None): 
    """ Runs the query for the graph Two """
    #TODO:TRY pyspark.sql https://stackoverflow.com/a/56279311
    
    # get first and last date to query from
    startDate = given_date
    endDate = given_date + relativedelta(months=1) - relativedelta(days=1)
    
    cur = conn.cursor()
    log.logging.info(f'Searching for amount of transactions over time with postcode:{postcode} on {given_date} starting on:{startDate} to {endDate}')
    
    # format query
    query = sql.SQL("SELECT price FROM price_paid \
                    where date_transfer between (%s) and (%s) \
                    and postcode like (%s)")
    log.logging.debug(query.as_string(conn))
    
    # run query
    cur.execute(query, [startDate.strftime("%m/%d/%Y"), endDate.strftime("%m/%d/%Y"), '%' + postcode + '%'])

    # display the PostgreSQL database server version
    result = cur.fetchall()
    log.logging.info(result)
    return result


if __name__ == '__main__':
    from datetime import datetime
    startDate = datetime(1990,1,1,0,0,0,0)
    endDate = datetime(1999,1,1,0,0,0,0)
    #query_graph_one("BH12 2AE", startDate, endDate,)

    given_date = datetime(2000,1,1,0,0,0,0)
    query_graph_two("SW18", given_date)
