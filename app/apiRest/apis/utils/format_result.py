# -*- encoding: utf-8 -*-
import numpy as np

from apiRest.apis.utils.connect import query_graph_one, query_graph_two

def format_result_graph_one(result):
    """Formats the output for graph one to meet the API Model"""
    result_formatter = dict()
    
    # sanity check if the database doesnt return anything
    if result and len(result) > 0:
        for x in result:
            if x[2] not in result_formatter.keys():
                result_formatter[x[2]]=list()

            tmp = dict()
            tmp["year"]=int(x[0])
            tmp["month"]=int(x[1])
            tmp["avg_price"]=float(x[3])
            result_formatter[x[2]].append(tmp)

    return result_formatter


def get_bucket_count(price_list, function="auto"):
    """Function to generate the bucket rane"""
    function = function if function in [ "auto", "fd", "scott", "rice", "sturges", "doane", "sqrt" ] else "auto"
    bucket_range = np.histogram_bin_edges(price_list, bins=function, range=(min(price_list), max(price_list)+1)) # random buckets
    return bucket_range


def format_result_graph_two(result):
    """Formats the output for graph two to meet the API Model"""
    bucket = dict()
    
    # sanity check if the database doesnt return anything
    if result and len(result) > 0:
        tmp_list = [ x[0] for x in result ]
        bucket_range = get_bucket_count(tmp_list)
        bucket_counter = 1

        for x in range(len(bucket_range)-1):
            bucket1 = dict()
            bucket1["Init_Range"] = bucket_range[x]
            bucket1["End_Range"] = bucket_range[x+1]
            bucket1["Amount"] = 0

            for y in tmp_list:
                if y >= bucket_range[x] and y < bucket_range[x+1]:
                    bucket1["Amount"] = bucket1.get("Amount", 0) + 1

            bucket[bucket_counter] = bucket1
            bucket_counter+=1

    return bucket

if __name__ == "__main__":
    """
    from datetime import datetime
    import pprint
    startDate = datetime(1995,1,1,0,0,0,0)
    endDate = datetime(2020,1,1,0,0,0,0)
    #res = query_graph_one("SW18 1AX", startDate, endDate,)
    #pprint.pprint(format_result_graph_one(res))

    #res = query_graph_two("SW18", startDate,)
    #pprint.pprint(format_result_graph_two(res))
    """
    print(get_bucket_count([1,2,3,4,5,6,7,8,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9, 1000], "auto"))