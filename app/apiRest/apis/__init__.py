# -*- encoding: utf-8 -*-
from flask import Blueprint
from flask_restplus import Api

from .api_jcn import api

blueprint = Blueprint('api', __name__, url_prefix='/api_v1.0')
api_v1 = Api(blueprint)

api_v1.add_namespace(api) 
