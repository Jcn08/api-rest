# -*- encoding: utf-8 -*-
from datetime import datetime

from apiRest.lib import my_logging as log
from apiRest.apis.utils.connect import query_graph_one, query_graph_two
from apiRest.apis.utils.format_result import format_result_graph_one, format_result_graph_two

from flask_restplus import Namespace, Resource, fields, reqparse

api = Namespace('graph', description='Provides data for frontend app')

#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#-*-*-*-*-*      API to retrieve DATA for GRAPH ONE  -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# Data Model
model_graph_one_tmp = api.model('', {
  'year': fields.Integer(required=True, description="Year where the average price is calculated"),
  'month': fields.Integer(required=True, description="Month where the average price is calculated"),
  'avg_price': fields.Float(required=True, description="Average Price for a certain property type on a given date"),
})

model_graph_one = api.model('Data Result Model for Graph One', {
  "D" : fields.List(fields.Nested(model_graph_one_tmp), required=False, description="Detached"),
  "S" : fields.List(fields.Nested(model_graph_one_tmp), required=False, description="Semi-detached"),
  "T" : fields.List(fields.Nested(model_graph_one_tmp), required=False, description="Terraced"),
  "F" : fields.List(fields.Nested(model_graph_one_tmp), required=False, description="Flat")
})
log.logging.debug("API Model defined for graph one")

# Request Arguments
parser_graph_one = api.parser()
parser_graph_one.add_argument('startDate', type=lambda x: datetime.strptime(x,'%b %Y'), required=True, location='args')
parser_graph_one.add_argument('endDate', type=lambda x: datetime.strptime(x,'%b %Y'), required=True, location='args')
parser_graph_one.add_argument('postcode', type=str, required=True, help='Postcode to retrieve the properties', location='args')

# EndPoint
@api.route('/one')
@api.param('startDate', 'Jan 1999')
@api.param('endDate', 'Feb 2011')
@api.param('postcode', 'Postcode to retrieve the properties')
class GraphOne(Resource):
    @api.doc('Get properties relative to a postcode on a certain given period of time')
    @api.marshal_with(model_graph_one)
    def get(self):
        """Get properties relative to a postcode on a certain given period of time"""
        log.logging.info("Get properties relative to a postcode on a certain given period of time")
        args = parser_graph_one.parse_args()
        return format_result_graph_one(query_graph_one(args['postcode'], args['startDate'], args['endDate']))



#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#-*-*-*-*-*      API to retrieve DATA for GRAPH TWO  -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# Data Model
# TODO

# Request Arguments
parser_graph_two = api.parser()
parser_graph_two.add_argument('Date', type=lambda x: datetime.strptime(x,'%b %Y'), required=True, location='args')
parser_graph_two.add_argument('postcode', type=str, required=True, help='Postcode to retrieve the properties', location='args')

# EndPoint
@api.route('/two')
@api.param('Date', 'Jan 1999')
@api.param('postcode', 'Postcode to retrieve the properties')
class GraphTwo(Resource):
    @api.doc('Get amount of transactions in a postcode on a certain given period of time')
    #@api.marshal_with(model_graph_two) #TODO
    def get(self):
        """Get amount of transactions in a postcode on a certain given period of time"""
        log.logging.info("Get amount of transactions in a postcode on a certain given period of time")
        args = parser_graph_two.parse_args()
        return format_result_graph_two(query_graph_two(args['postcode'], args['Date']))
#-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*