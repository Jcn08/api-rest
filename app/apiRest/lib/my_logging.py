# -*- encoding: utf-8 -*-
import os
import json

import datetime

import logging.config

#/path/to/package/lib/my_logging.py
dir_abs = os.path.abspath(__file__)
#/path/to/package/lib
dir_name = '/'.join(dir_abs.split('/')[:-1])
#path/to/package
package_name = '/'.join(dir_name.split('/')[:-1])
#package
nameScript = package_name.split('/')[-1]

class MyLogging:
    def __init__(self):
        self.cfgFile = os.path.join(dir_name, 'cfg/logging.cfg')
        self.confJson = self.getConf()
        self.load_conf_json()

    def getConf(self):
        with open(self.cfgFile, "r", encoding="utf-8") as fd:
            return json.load(fd)

    def load_conf_json(self):
        try:
            if os.environ['LOGUTIL']:
                self.LogDIR = str(os.environ['LOGUTIL'])
        except KeyError as err:
            self.LogDIR = os.path.join(dir_name, 'log')

        date = datetime.date.today().strftime('_%Y-%m-%d')

        infoname = "apiRest.log"
        errorname = "apiRest.err"

        if os.path.isdir(self.LogDIR):
            self.confJson["handlers"]["info_file_handler"]["filename"] =\
                os.path.join(self.LogDIR, "../..", infoname)
            self.confJson["handlers"]["error_file_handler"]["filename"] =\
                os.path.join(self.LogDIR, "../..", errorname)

        else:
            self.confJson["handlers"]["info_file_handler"]["filename"] = \
                os.path.join(package_name, "..", 
                self.confJson["handlers"]["info_file_handler"]["filename"])
            self.confJson["handlers"]["error_file_handler"]["filename"] = \
                os.path.join(package_name, "..",
                self.confJson["handlers"]["error_file_handler"]["filename"])

        logging.config.dictConfig(self.confJson)


a = MyLogging()

if __name__ == '__main__':
    a = MyLogging()
