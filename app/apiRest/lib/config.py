# -*- encoding: utf-8 -*-
import os

from configparser import ConfigParser

from .my_logging import logging

#/path/to/package/lib/my_logging.py
dir_abs = os.path.abspath(__file__)
#/path/to/package/lib
dir_name = '/'.join(dir_abs.split('/')[:-1])
#path/to/package
package_name = '/'.join(dir_name.split('/')[:-1])
#package
nameScript = package_name.split('/')[-1]

def config(filename=os.path.join(dir_name, 'cfg/config_db.ini'), section='postgresql'):
    parser = ConfigParser()
    parser.read(filename)

    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        logging.error("Error when connecting to database")
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return db
