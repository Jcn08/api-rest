# -*- encoding: utf-8 -*-
"""This module starts and stops the FLASK APP."""

import sys

from flask import Flask

from apiRest.apis import blueprint as api
from apiRest.lib import my_logging as log

log.logging.info("Starting Flask app")

try:
    app = Flask(__name__) 
    app.register_blueprint(api)
    log.logging.info("app succesfully started")

except:
    log.logging.error("the FLASK app couldn't start", sys.exc_info()[2])

